public class Person {

    //the below code defines our person class

    //object attributes
    private String forename;
    private String surname;
    private Pet myPet;

    //constructor
    public Person (String fName, String sName, Pet myPet) {
        this.forename = fName;
        this.surname = sName;
        this.myPet = myPet;
    }

    //getter methods
    public String getForename() {
        return this.forename;
    }

    public String getSurname() {
        return this.surname;
    }

    public String getFullName() {
        return forename + " " + surname;
    }

    //setter methods
    public void setForename(String forename) {
        this.forename = forename;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setMyPet(Pet myPet) {
        this.myPet = myPet;
    }

    public Pet getMyPet() {
        return myPet;
    }
}
