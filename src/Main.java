public class Main {

    public static void main(String[] args) {
        Pet teddy = new Pet("Teddy", "Dog");
        Pet mrSnake = new Pet("Mr Snake", "Serpent");
        Pet milo = new Pet("Milo", "Cat");

        Person adam = new Person("Adam", "Genesis", null);
        Person eve = new Person("Eve", "Genesis", mrSnake);
        Person celia = new Person("Celia", "Perry", teddy);
        Person rohan = new Person("Rohan", "George", milo);

        eve.setMyPet(mrSnake);
        System.out.println(adam.getFullName());
        System.out.println(eve.getFullName());

        eve.setSurname("Snake");

        System.out.println(eve.getFullName());

        Pet theirPet = eve.getMyPet();

        if (theirPet == null) {
            System.out.println(eve.getForename() + " doesn't have a pet");
        }
        else {
            System.out.println(eve.getForename() + " has a pet called " + theirPet.getName());
        }
    }
}
